import flask
from flask import Flask, jsonify, request, render_template
from flask_cors import CORS
from random import seed, randint

import fake_news
from fake_news import *

seed(1)
app = Flask(__name__)
CORS(app)

@app.route('/')
def home_page():
    example_embed='Simple server page'
    return render_template('index.html', embed=example_embed)

@app.route('/test', methods=['GET', 'POST'])
def testfn():
    if request.method == 'GET':
        message = {'message':'GET method'}
        return jsonify(message)
    if request.method == 'POST':
        print(request.data)
        relevancy = fake_news.analyze_news(vect, model, request.data)
        print(relevancy)
        message = flask.jsonify({'result': int(relevancy)})
        message.headers.add('Access-Control-Allow-Origin', '*')
        return message

vect, model = fake_news.initialise()
app.run(debug=True)