import pandas as pd
from joblib import load
import sklearn

def initialise():
    vct = load('vectorizer.joblib')
    mdl = load('model.joblib')
    return vct, mdl


def output_label(n):
    if n == 0:
        return "Fake News"
    elif n == 1:
        return "Not A Fake News"


def analyze_news(vectorizer, model, news):
    testing_news = {"text":[news]}
    new_def_test = pd.DataFrame(testing_news)
    test = new_def_test["text"]


    test_vector = vectorizer.transform(test)
    prediction = model.predict(test_vector)
    print(prediction)
    return prediction[0]

