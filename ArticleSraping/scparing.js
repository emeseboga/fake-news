
var articleWrapper = document.getElementsByClassName('js_post-content')[0];

fetch('http://localhost:5000/test', {
  method: 'POST',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json;charset=UTF-8'
  },
  cors: 'no-cors',
  body: JSON.stringify(articleWrapper.textContent)
})
  .then(response => {
    return response.json();
  }).then(function(text) {
    chrome.runtime.sendMessage(articleWrapper.innerHTML + ' ' + `Relevancy: ${text.result}`); 
  });

